FROM registry.gitlab.com/logius/beveiligd-koppelvlak-extern/base-images/eclipse-temurin:17-jre-alpine-opentelemetry

#See https://gitlab.com/logius/beveiligd-koppelvlak-extern/base-images/eclipse-temurin
ARG ALPINE_VERSION=v3.18

ENV ACTIVEMQ_VERSION 5.18.3
ENV ACTIVEMQ apache-activemq-$ACTIVEMQ_VERSION
ENV ACTIVEMQ_TCP=61616 ACTIVEMQ_AMQP=5672 ACTIVEMQ_STOMP=61613 ACTIVEMQ_MQTT=1883 ACTIVEMQ_WS=61614 ACTIVEMQ_UI=8161 ACTIVEMQ_JMX=10051
ENV ACTIVEMQ_HOME /opt/activemq
ENV ACTIVEMQ_USER_UID 666

ARG POSTGRES_DRIVER_VERSION=42.6.0
ARG POSTGRES_DRIVER=postgresql-${POSTGRES_DRIVER_VERSION}.jar

ARG HIKARICP_DRIVER_VERSION=5.1.0
ARG HIKARICP_DRIVER=HikariCP-${HIKARICP_DRIVER_VERSION}.jar
ARG M2_REPO_URL=https://repo1.maven.org/maven2

USER root

RUN apk add \
      --no-cache \
      --repository http://dl-cdn.alpinelinux.org/alpine/${ALPINE_VERSION}/main \
      curl
RUN getIt() { curl -kLS --retry-all-errors --retry 10 --retry-delay 1 ${1} -o ${2}; } && \
  getIt https://archive.apache.org/dist/activemq/${ACTIVEMQ_VERSION}/${ACTIVEMQ}-bin.tar.gz ${ACTIVEMQ}-bin.tar.gz && \
  getIt https://jdbc.postgresql.org/download/${POSTGRES_DRIVER} ${POSTGRES_DRIVER} && \
  getIt https://repo1.maven.org/maven2/com/zaxxer/HikariCP/${HIKARICP_DRIVER_VERSION}/${HIKARICP_DRIVER} ${HIKARICP_DRIVER} && \
  tar xvzf ${ACTIVEMQ}-bin.tar.gz -C  /opt && \
  mv /opt/${ACTIVEMQ} ${ACTIVEMQ_HOME} && \
  mv ${POSTGRES_DRIVER} ${ACTIVEMQ_HOME}/lib/${POSTGRES_DRIVER} && \
  mv ${HIKARICP_DRIVER} ${ACTIVEMQ_HOME}/lib/${HIKARICP_DRIVER} && \
  rm ${ACTIVEMQ}-bin.tar.gz

RUN apk del \
      --no-cache \
      --repository http://dl-cdn.alpinelinux.org/alpine/${ALPINE_VERSION}/main \
      curl

COPY config/activemq.xml /opt/activemq/conf/activemq.xml
COPY config/credentials.properties /opt/activemq/conf/credentials.properties

# Create user to comply to numeric UID for k8s. This workaround should be adopted in the base image.
RUN addgroup activemq && adduser -S -h ${ACTIVEMQ_HOME} -G activemq -u ${ACTIVEMQ_USER_UID} activemq && \
  chown -R activemq:activemq /opt/activemq && \
  chown -h activemq:activemq ${ACTIVEMQ_HOME}

USER ${ACTIVEMQ_USER_UID}
WORKDIR ${ACTIVEMQ_HOME}
EXPOSE ${ACTIVEMQ_TCP} ${ACTIVEMQ_AMQP} ${ACTIVEMQ_STOMP} ${ACTIVEMQ_MQTT} ${ACTIVEMQ_WS} ${ACTIVEMQ_UI} ${ACTIVEMQ_JMX}

# set admin portal host, else it's not reachable externally
RUN sed -i "s/127.0.0.1/0.0.0.0/g" conf/jetty.xml

CMD ["/bin/sh", "-c", "bin/activemq console"]