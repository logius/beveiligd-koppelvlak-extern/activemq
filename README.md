# Apache ActiveMQ & OSDBK

Apache ActiveMQ is part of OSDBK (Open Source Dienst Beveiligd Koppelvlak). OSDBK consists of seven applications:

- EbMS Core / EbMS Admin
- JMS Producer
- JMS Consumer
- Apache ActiveMQ
- CPA Service
- OSDBK Admin Console
- Throttling Service (optional)

Apache ActiveMQ is used as middleware between backend applications and OSDBK, which enables the asynchronous message processing that ebms is built on.

For each incoming EbMS Message, EbMS Core sends an event to the RECEIVED queue on ActiveMQ, that is then picked up by the JMS Producer and routed to the correct queue.

Outbound messages are received from the JMS Consumer, that reads them a number of configurable queues of ActiveMQ. 

EbMS Core will create a Delivery Task for the outgoing message, which is sent to the DELIVERY_TASK queue.


Apache ActiveMQ supports an XML deployment descriptor for configuring the ActiveMQ Message Broker.
There are many things which can be configured such as
~~~
- transport connectors which consist of transport channels and wire formats
- network connectors using network channels or discovery agents
- persistence providers & locations
- custom message containers (such as last image caching etc)
~~~

An example deployment descriptor has been provided in `config/activemq.xml`

This file needs to be configured to meet a user's specific needs.

### How to run this application locally
An example docker-compose has been provided. This can be run to test this application.
However the following variables need to be populated by means of a `.env` file:
~~~
${ACTIVEMQ_POSTGRESQL_USERNAME} -- the username used to connect to the ActiveMQ database
${ACTIVEMQ_POSTGRESQL_PASSWORD} -- the password used to connect to the ActiveMQ database
~~~

Example command to bring up the application:
~~~
docker-compose --env-file path/to/env/file/.env up
~~~

## Release notes
See [NOTES][NOTES] for latest.

[NOTES]: templates/NOTES.txt

### Older release notes collated below:

V Up to 5.18.3
- Various improvements to:
    - container technology used
    - use of base-images
    - GITLAB Security scan framework implemented
    - Improved Open Source build process and container releases
    - Test improvements via docker-compose
